﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Tp01part2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            imgRedigerMessage.Source = ImageSource.FromResource("Tp01part2.Ressources.pen.png");
            imgRechercherMessage.Source = ImageSource.FromResource("Tp01part2.Ressources.search.png");
            imgRepondreMessage.Source = ImageSource.FromResource("Tp01part2.Ressources.back.png");
            imgPartagerMessage.Source = ImageSource.FromResource("Tp01part2.Ressources.refresh.png");
            imgAjoutFavorisMessage.Source = ImageSource.FromResource("Tp01part2.Ressources.star.png");

        }



    }
}
